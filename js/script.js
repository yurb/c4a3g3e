// License: CC0
// https://creativecommons.org/publicdomain/zero/1.0/

$(document).ready(function () {
    "use strict";

    /* Config */
    var vidw = 400;
    var vidh = 600;
    var rotstep = 20;
    var playSym = '▶';
    var pauseSym = '▮▮';

    /* State */
    var currot = 0;
    var playing = false;

    /* DOM elements */
    var video = $('#srcvid').get(0); // The raw JS object instead of JQuery one
    var container = $('.carousel');
    var slider = $('#slider');
    var canvases = $('#videos canvas');
    var playpause = $('.button-playpause');
    var bufferingMsg = $('#buffering-message');
    var noSupportWarning = $('.warning.nosupport');
    var linkToVideo = $('.warning.link-to-video');
    var controls = $('#controls');

    /* Compatibility */
    var noSupport = function () {
        $(controls).add(bufferingMsg).hide();
        $(noSupportWarning).add(linkToVideo).show();
    };
    if((Modernizr.video.webm || Modernizr.video.h264) &&
       Modernizr.csstransforms3d &&
       Modernizr.canvas) {
        // Browser has enough html5/css3 support
        /* Split video and draw to the canvases */
        var contexts = canvases.map(function () {
            var context = this.getContext("2d");
            return context;
        }).get();
        canvases.each(function () {
            this.width = vidw;
            this.height = vidh;
        });
        var updateDisplay = function () {
            contexts.forEach(function (ctx, i) {
                ctx.drawImage(video, vidw * i, 0, vidw, vidh, 0, 0, vidw, vidh);
            });
            if (video.paused == false) {
                requestAnimationFrame(updateDisplay);
            }
        };
        $(video).on("loadeddata play seeked", function () {
            updateDisplay();
        });
        
        /* Rotation actions */
        var rotate = function (degrees) {
            currot = currot + degrees;
            container.css('transform', "rotateY(" + currot + "deg)");
        };
        var rotateL = function () {
            rotate(rotstep * -1);
        };
        var rotateR = function () {
            rotate(rotstep);
        };

        /* Playback actions */
        var vidPlay = function () {
            video.play();
        };
        var vidPause = function () {
            video.pause();
        };
        var vidPlayOrPause = function () {
            if (video.paused) {
                vidPlay();
                playpause.trigger('playing');
            } else {
                vidPause();
                playpause.trigger('paused');
            }
        };

        /* Playback controls */
        // Button actions
        $('#controls button').button();
        
        // Custom events for reusability
        playpause.on('playing', function () {
            var $this = $(this);
            $this.button('option', 'label', pauseSym);
        });
        playpause.on('paused', function () {
            var $this = $(this);
            $this.button('option', 'label', playSym);
        });
        playpause.on('click', function (ev) {
            vidPlayOrPause();
        });

        // Buffering visual feedback
        $(video).on("stalled waiting", function () {
            bufferingMsg.show();
        }).on("canplay playing canplaythrough", function () {
            bufferingMsg.hide();
        });

        // When the video ends, update the button state
        $(video).on("ended", function () {
            playpause.trigger('paused');
        });

        // Since we use the arrow keys to rotate the view, we need the slider widget without keyboard events
        $.widget("ui.slidernokeys", $.ui.slider, {
            _handleEvents: function (event) {
                // Do nothing
            }
        });

        // Initialize the slider widget
        slider.slidernokeys({
            min: 0,
            max: 343, // The duration of the videos in seconds
            slide: function (event, ui) {
                video.currentTime = ui.value;
            }
        });

        // Update the slider periodically based on the first video's time
        $(video).on('timeupdate', function () {
            slider.slidernokeys("value", this.currentTime);
        });


        /* Rotation controls */
        $('.button-rotl').on('click', function (ev) {
            ev.preventDefault;
            rotateL();
        });
        $('.button-rotr').on('click', function (ev) {
            ev.preventDefault;
            rotateR();
        });

        // Keyboard
        $('body').on("keydown", function (ev) {
            ev.preventDefault;
            switch (ev.which) {
            case 37: // Left arrow
                rotateL();
                break;
            case 39:
                rotateR();
                break;
            case 32: // Space bar
                vidPlayOrPause();
                break;
            }
        });
    } else {
        // Browser's missing html5/css3 support
        noSupport();
    }
});
